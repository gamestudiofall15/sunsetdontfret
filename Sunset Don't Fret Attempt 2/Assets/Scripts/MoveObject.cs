﻿using UnityEngine;
using System.Collections;

public class MoveObject : MonoBehaviour {
	
	//vector2 velocity effects direction and speed of moving objects
	Rigidbody2D rb;
	Vector2 velocity = new Vector2 (-2,0);
	
	void Start(){
		// set reference to the rigid body 
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
		rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
	}
}
