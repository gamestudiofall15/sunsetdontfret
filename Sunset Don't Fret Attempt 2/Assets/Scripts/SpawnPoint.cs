﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour {
	
	public GameObject[] prefabs;
	
	//effects the speed at which object spwans
	//also creates option in unity to set spawn rate. don't need the 0.8f but its fine either way. 
	public float spawnRate;
	
	// Use this for initialization
	void Start () {
		InvokeRepeating("NewRandomPrefab" , 0, spawnRate );
	}
	
	void NewRandomPrefab(){
		
		int randomIndex = Random.Range(0, prefabs.Length  );
		
		GameObject prefabToSpawn = prefabs[ randomIndex ]; 
		
		
		Instantiate( prefabToSpawn , gameObject.transform.position, Quaternion.identity );
	}
}
