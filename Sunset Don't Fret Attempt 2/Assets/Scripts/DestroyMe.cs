﻿using UnityEngine;
using System.Collections;

public class DestroyMe : MonoBehaviour {
	
	public float timeToDie = 15.0f;
	
	// Use this for initialization
	void Start () {
		GameObject.Destroy(this.gameObject, timeToDie );
	}
	
	
}

